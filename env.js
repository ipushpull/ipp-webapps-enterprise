const ENV = {
  ipushpull: {
    api_url: "https://api-ent.ipushpull.com/api",
    auth_url: "https://auth-ent.ipushpull.com",
    app_url: "https://enterprise.ipushpull.com",
    api_key: "ijAe5mCqDU6nX2STKxAYzJFbgy6WdMXfOcLlxLoo",
    api_secret: "bIySqp0Xkb2RNlWVonioMM9rDtNJpVEaJLjhUfMVknBGOol2oxuFGhTlcKEJ4M4UqxyE2KJfTZQP9z9fY1zFSuHny9EXXIYPLUs4fmDakeFuZIpIrOk1I4ua1I5taBzL",
    hsts: false,
    storage_prefix: "ipp-enterprise",
    storage_host: "ipushpull.com",
    transport: "polling",
    client_version: "",
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: "auth",
          },
          images: [],
          logo: "enterprise/img/logo-light.svg",
        },
        embed: {
          ipushpull: {
            client_version: "embed",
          },
        },
        mobile: {
          ipushpull: {
            storage_prefix: "ipp_enterprise_mobile",
            client_version: "mobile",
            api_key: "",
            api_secret:
              "",
          },
          client_application: "mobile",
        },
        client: {
          ipushpull: {
            client_version: "client",
          },
          stripe: {
            key: "",
          },
          dod: true,
          theme: {
            upgrade: {
              version: "2021.12.2",
              type: "minor",
              release: {
                label: "Find out more",
                help_doc: "https://support.ipushpull.com",
              },
              message:
                "<p>There's a new version of the ipushpull app available, which includes:</p><ul><li>Streamlined onboarding for new and invited users</li><li>Microsoft Teams and Slack integrations coming soon - get in touch for a preview</li></ul>",
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-new.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-white.svg",
              },
            },
            ui: {
              nav_workspaces: true,
              nav_folders: true,
              nav_create: true,
              nav_help: true,
              nav_share: true,
              nav_setup: true,
              nav_setup_lsd: true,
              nav_setup_dod: true,
              nav_setup_ddn: true,
              nav_setup_caw: true,
              nav_setup_admin: true,

              profile_tab_integrations: true,
              profile_tab_keys: true,

              folders_and_pages_types: [],
              folders_and_pages_can_pin_folder: true,
              folders_and_pages_can_leave_folder: true,
              folders_and_pages_tab_recent: true,
              folders_and_pages_tab_favs: true,

              toolbar: true,
              toolbar_fav: true,
              toolbar_autosave: true,
              toolbar_columns: true,
              toolbar_highlights: true,
              toolbar_tracking: true,
              toolbar_filters: true,
              toolbar_sort: true,
              toolbar_views: true,
              toolbar_share: true,

              toolbar_menu_view: true,
              toolbar_menu_view_fit: true,
              toolbar_menu_view_fit_scroll: true,
              toolbar_menu_view_fit_width: true,
              toolbar_menu_view_fit_height: true,
              toolbar_menu_view_fit_contain: true,

              toolbar_menu_view_show: true,
              toolbar_menu_view_show_headings: true,
              toolbar_menu_view_show_row_selection: true,
              toolbar_menu_view_show_row_highlight: true,
              toolbar_menu_view_show_gridines: true,
              toolbar_menu_view_show_cell_bar: true,
              toolbar_menu_view_show_filters: true,
              toolbar_menu_view_freeze: true,

              toolbar_menu_data: true,

              toolbar_menu_page: true,

              page_views: true,

              grid_menu_copy: true,
              grid_menu_filter: true,
              grid_menu_sort: true,
              grid_menu_freeze: true,
              grid_menu_rowscols: true,
              grid_menu_cell: true,
              grid_menu_share: true,
            },
            logo: {
              light: "enterprise/img/logo.svg",
              dark: "enterprise/img/logo.svg",
            },
          },
          services: [
            {
              name: "Desktop",
              key: "ipp",
              serviceType: ["lsd"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Mobile",
              key: "mobile",
              serviceType: ["lsd"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Excel Add-in",
              key: "excel",
              serviceType: ["lsd"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
            },
            {
              name: "Microsoft Teams",
              key: "teams",
              botKey: "teamsBot",
              serviceType: ["lsd", "dod", "ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
            },
            {
              name: "Symphony",
              key: "symphony",
              botKey: "symphonyBot",
              serviceType: ["lsd", "dod", "ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
            },
            {
              name: "Slack",
              key: "slack",
              serviceType: ["dod", "ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
            },
            {
              name: "WhatsApp",
              key: "whatsapp",
              serviceType: ["ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/whatsapp.png",
            },
            {
              name: "SMS",
              key: "sms",
              serviceType: ["ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/sms.png",
            },
            {
              name: "Email",
              key: "email",
              serviceType: ["ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/email.png",
            },
          ],
        },
      },
    },
  },
  sso_only: false,
  sso_url: "",
  on_prem: true,
  billing: false,
  help_docs: {
    livestreaming_data_source_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
      iframe: "https://test.ipushpull.com/help.php?id=647790593",
    },
    livestreaming_data_source_db: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
      iframe: "https://test.ipushpull.com/help.php?id=148865025",
    },
    livestreaming_data_source_api: {
      desk: "https://ipushpull.atlassian.net/wiki/x/CYCgK",
      iframe: "https://test.ipushpull.com/help.php?id=681607177",
    },
    livestreaming_client_app_ipp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
      iframe: "https://test.ipushpull.com/help.php?id=681705505",
    },
    livestreaming_client_app_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
      iframe: "https://test.ipushpull.com/help.php?id=156074055",
    },
    livestreaming_client_app_symphony: {
      desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
      iframe: "https://test.ipushpull.com/help.php?id=695959587",
    },
    livestreaming_client_app_wp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
      iframe: "https://test.ipushpull.com/help.php?id=696352875",
    },
    livestreaming_client_app_teams: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
      iframe: "https://test.ipushpull.com/help.php?id=678494209",
    },
    notifications_builder_excel_data_source: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDmI",
      iframe: "https://test.ipushpull.com/help.php?id=551976961",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/H4BeHg",
      iframe: "https://test.ipushpull.com/help.php?id=509509663",
    },
    notifications_builder_slack_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/JgBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542438",
    },
    notifications_builder_symphony_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/LwBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542447",
    },
    symphony_overview: {
      desk: "https://ipushpull.atlassian.net/wiki/x/UYCHDQ",
      iframe: "https://test.ipushpull.com/help.php?id=226984017",
    },
    user_authoriziations: {
      desk: "https://ipushpull.atlassian.net/l/c/GApuWLAA",
      iframe: "https://test.ipushpull.com/help.php?id=631701505",
    },
  },
};
